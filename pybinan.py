import tkinter as tk
import time
import logging
import matplotlib.pyplot as plt
from binance.client import Client
from binance.websockets import BinanceSocketManager

# Imposta il logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# Inizializza il client Binance
client = Client("API_KEY", "API_SECRET")

# Inizializza il Binance Socket Manager
bm = BinanceSocketManager(client)

# Carica lo stato del bot dal file di registro, se esiste
try:
    with open("gridbot_state.txt", "r") as f:
        symbol, grid_size, grid_spacing = f.read().split(",")
        grid_size = float(grid_size)
        grid_spacing = float(grid_spacing)
except FileNotFoundError:
    # Definire le impostazioni del gridbot (inizialmente impostate su BTCUSDT)
    symbol = "BTCUSDT"
    grid_size = 0.5  # la dimensione del grid, in BTC
    grid_spacing = 0.1  # lo spaziamento del grid, in BTC

# Crea la finestra di tkinter
window = tk.Tk()
window.title("Gridbot Trading")

# Crea la casella di testo per la coppia di trading
symbol_label = tk.Label(text="Coppia di trading:")
symbol_entry = tk.Entry()
symbol_entry.insert(0, symbol)

# Crea la casella di testo per la dimensione del grid
grid_size_label = tk.Label(text="Dimensione del grid (in BTC):")
grid_size

# Crea il pulsante per inviare gli ordini di grid
def submit_grid():
    global symbol, grid_size, grid_spacing
    symbol = symbol_entry.get()
    grid_size = float(grid_size_entry.get())
    grid_spacing = float(grid_spacing_entry.get())

    # Salva lo stato del bot nel file di registro
    with open("gridbot_state.txt", "w") as f:
        f.write(",".join([symbol, str(grid_size), str(grid_spacing)]))

    # Ottieni il prezzo attuale di BTCUSDT
    ticker = client.fetch_ticker(symbol)
    price = ticker["last"]

    # Calcola i prezzi di acquisto e vendita per ogni livello del grid
    buy_prices = [price - (i * grid_spacing) for i in range(int(grid_size / grid_spacing))]
    sell_prices = [price + (i * grid_spacing) for i in range(int(grid_size / grid_spacing))]

    # Calcola il capitale necessario per inviare gli ordini di grid
    capital_required = (grid_size / grid_spacing) * 2 * price

    # Mostra il capitale necessario e il saldo attuale
    balance = client.fetch_balance()
    capital_label.config(text="Capitale necessario: {:.2f} {}\nSaldo attuale: {:.2f} {}".format(capital_required, symbol[3:], float(balance[symbol[3:]]['free']), symbol[3:]))

    # Invia gli ordini di acquisto e vendita al livello del grid
    for p in buy_prices:
        client.create_order(
            symbol=symbol,
            side=Client.SIDE_BUY,
            type=Client.ORDER_TYPE_LIMIT,
            price=str(p),
            quantity="1"
        )

    for p in sell_prices:
        client.create_order(
            symbol=symbol,
            side=Client.SIDE_SELL,
            type=Client.ORDER_TYPE_LIMIT,
            price=str(p),
            quantity="1"
        )

    logging.info("Ordini di grid inviati per {}".format(symbol))

submit_button = tk.Button(text="Invia ordini di grid", command=submit_grid)

# Crea la label per il capitale necessario e il saldo attuale
capital_label = tk.Label(text="")

# Crea il pulsante per visualizzare gli ordini aperti
def view_open_orders():
    open_orders = client.fetch_open_orders(symbol=symbol)
    open_orders_label.config(text="Ordini aperti:\n")
    for o in open_orders:
        open_orders_label.

# Crea la label per gli ordini aperti
open_orders_label = tk.Label(text="")

# Crea il pulsante per visualizzare gli ordini chiusi
def view_closed_orders():
    closed_orders = client.fetch_closed_orders(symbol=symbol)
    closed_orders_label.config(text="Ordini chiusi:\n")
    for o in closed_orders:
        closed_orders_label.config(text=closed_orders_label.cget("text") + str(o) + "\n")

view_closed_orders_button = tk.Button(text="Visualizza ordini chiusi", command=view_closed_orders)

# Crea la label per gli ordini chiusi
closed_orders_label = tk.Label(text="")

# Crea il pulsante per visualizzare le perdite e i ricavi del bot
def view_profit_loss():
    open_orders = client.fetch_open_orders(symbol=symbol)
    closed_orders = client.fetch_closed_orders(symbol=symbol)

    # Calcola le perdite e i ricavi delle operazioni aperte
    open_profit_loss = 0
    for o in open_orders:
        open_profit_loss += o["price"] * o["quantity"]

    # Calcola le perdite e i ricavi delle operazioni chiuse
    closed_profit_loss = 0
    for o in closed_orders:
        closed_profit_loss += o["price"] * o["quantity"]

    profit_loss_label.config(text="Perdite/ricavi:\nAperte: {:.2f} {}\nChiuse: {:.2f} {}".format(open_profit_loss, symbol[3:], closed_profit_loss, symbol[3:]))

view_profit_loss_button = tk.Button(text="Visualizza perdite/ricavi", command=view_profit_loss)

# Crea la label per le perdite e i ricavi
profit_loss_label = tk.Label(text="")

# Crea la finestra di plot
plt.ion()
fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)

# Inizializza la lista dei prezzi
prices = []

# Definire la funzione di callback per il socket di candeliere Kline
def process_candlestick(msg):
    # Aggiungi il prezzo alla lista dei prezzi
    prices.append(float(msg["k"]["c"]))

    # Aggiorna il plot in tempo reale
    ax1.clear()
    ax1.plot(prices)
    plt.title("Prezzo di {}".format(symbol))
    plt.ylabel("Prezzo ({})".format(symbol[3:]))
    plt.xlabel("Tempo")
    plt.draw()

# Avvia il socket di candeliere Kline
bm.start_kline_socket(symbol, process_candlestick)
bm.start()

# Posiziona i widget nella finestra di tkinter
symbol_label.grid(row=0, column=0)
symbol_entry.grid(row=0, column=1)
grid_size_label.grid(row=1, column=0)
grid_size_entry.grid(row=1, column=1)
grid_spacing_label.grid(row=2, column=0)
grid_spacing_entry.grid(row=2, column=1)
submit_button.grid(row=3, column=0, columnspan=2)
capital_label.grid(row=4, column=0, columnspan=2)
view_open_orders_button.grid(row=5, column=0)
open_orders_label.grid(row=6, column=0, columnspan=2)
view_closed_orders_button.grid(row=7, column=0)
closed_orders_label.grid(row=8, column=0, columnspan=2)
view_profit_loss_button.grid(row=9, column=0)
profit_loss_label.grid(row=10, column=0, columnspan=2)

# Avvia la finestra di tkinter
window.mainloop()

# Arresta il socket di candeliere Kline
bm.stop_socket(symbol)
bm.stop()